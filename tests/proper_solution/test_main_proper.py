import pytest
import requests

BASE_URL = "http://proper_solution_app:5000"

def test_solve_endpoint_basic_functionality():
    # A known graph and its expected result for simplicity
    data = {
        "graph": [0, 10, 15, 20, 10, 0, 35, 25, 15, 35, 0, 30, 20, 25, 30, 0]
    }
    response = requests.post(f"{BASE_URL}/solve", json=data)
    assert response.status_code == 200
    json_response = response.json()
    assert "Minimum Cost" in json_response
    assert "Optimal Path" in json_response
    assert "Segment Costs" in json_response

