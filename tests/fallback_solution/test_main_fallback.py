import pytest
import requests

BASE_URL = "http://localhost:5000"

# 1. nearest_neighbor() Function Tests

def test_nearest_neighbor_basic():
    # Known graph and expected result
    graph = [
        [0, 29, 20, 21],
        [29, 0, 15, 17],
        [20, 15, 0, 28],
        [21, 17, 28, 0]
    ]
    response = requests.post(f"{BASE_URL}/solve", json={"graph": [item for sublist in graph for item in sublist]})
    assert response.status_code == 200
    json_response = response.json()
    assert "method" in json_response and json_response["method"] == "nearest_neighbor"
    # Additional assertions based on expected values can be added

# 2. tsp() Function Tests
def test_solve_endpoint_basic_functionality():
    # A known graph and its expected result for simplicity
    data = {
        "graph": [0, 10, 15, 20, 10, 0, 35, 25, 15, 35, 0, 30, 20, 25, 30, 0]
    }
    response = requests.post(f"{BASE_URL}/solve", json=data)
    assert response.status_code == 200
    json_response = response.json()
    assert "Minimum Cost" in json_response
    assert "Optimal Path" in json_response
    assert "Segment Costs" in json_response



# 3. /solve Endpoint Tests

def test_solve_endpoint_exact_solution():
    data = {
        "graph": [0, 10, 15, 20, 10, 0, 35, 25, 15, 35, 0, 30, 20, 25, 30, 0]
    }
    response = requests.post(f"{BASE_URL}/solve", json=data)
    assert response.status_code == 200
    json_response = response.json()
    assert "method" in json_response and json_response["method"] == "exact"
    # Additional assertions based on known expected values can be added

def test_solve_endpoint_nearest_neighbor():
    # An example graph with size greater than 15
    data = {
        "graph": # Insert a sufficiently large graph here
    }
    response = requests.post(f"{BASE_URL}/solve", json=data)
    assert response.status_code == 200
    json_response = response.json()
    assert "method" in json_response and json_response["method"] == "nearest_neighbor"


