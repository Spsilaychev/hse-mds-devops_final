from flask import Flask, jsonify, request

app = Flask(__name__)

def nearest_neighbor(graph):
    n = len(graph)
    unvisited = set(range(1, n))
    current_city = 0
    tour = [current_city]
    total_cost = 0

    while unvisited:
        nearest_city = min(unvisited, key=lambda city: graph[current_city][city])
        total_cost += graph[current_city][nearest_city]
        unvisited.remove(nearest_city)
        tour.append(nearest_city)
        current_city = nearest_city

    total_cost += graph[current_city][0]  # Return to the starting city
    tour.append(0)  # Complete the tour by returning to the start

    return total_cost, tour

def tsp(graph):
    n = len(graph)
    # memoization table, where dp[mask][i] stores the minimum cost to visit cities in mask, ending in city i
    dp = [[float('inf')] * n for _ in range(1 << n)]
    dp[1][0] = 0

    # For path reconstruction
    parent = [[-1] * n for _ in range(1 << n)]

    # Iterate through all subsets of vertices
    for mask in range(1, 1 << n):
        for u in range(n):
            # Continue if u is not in the subset represented by mask
            if not (mask & (1 << u)):
                continue

            # Try to find the shortest path to u from any vertex v
            for v in range(n):
                if mask & (1 << v) and u != v and dp[mask][u] > dp[mask ^ (1 << u)][v] + graph[v][u]:
                    dp[mask][u] = dp[mask ^ (1 << u)][v] + graph[v][u]
                    parent[mask][u] = v

    # Reconstruct the shortest path and compute the minimum cost
    mask = (1 << n) - 1  # All cities have been visited
    u = 0
    min_cost = min(dp[mask][v] + graph[v][u] for v in range(1, n))

    # Reconstructing the path
    v = min(range(1, n), key=lambda v: dp[mask][v] + graph[v][u])
    path = [0, v]
    costs = [graph[0][v]]
    while mask > 1:
        u, v = v, parent[mask][v]
        mask ^= (1 << u)
        if v != -1:  # To handle the case when we reach the starting point
            path.append(v)
            costs.append(graph[u][v])

    path.reverse()
    costs.reverse()
    return min_cost, path, costs

@app.route('/solve', methods=['POST'])
def solve():
    data = request.json

    # Convert the graph data from the JSON payload
    graph_data = data.get('graph')
    if not graph_data:
        return jsonify({"error": "Graph data not provided"}), 400

    n = int(len(graph_data) ** 0.5)
    graph = [graph_data[i:i+n] for i in range(0, len(graph_data), n)]

    # Validate input
    if len(graph) != n:
        return jsonify({"error": "Please provide a valid square matrix"}), 400

    if len(graph) <= 15:
        min_cost, optimal_path, segment_costs = tsp(graph)
        return jsonify({
            "method": "exact",
            "min_cost": min_cost,
            "optimal_path": optimal_path,
            "segment_costs": segment_costs
        })

    else:
        min_cost, optimal_path = nearest_neighbor(graph)
        return jsonify({
            "method": "nearest_neighbor",
            "min_cost": min_cost,
            "optimal_path": optimal_path
        })

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
