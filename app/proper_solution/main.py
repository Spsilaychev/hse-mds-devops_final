from flask import Flask, jsonify, request

app = Flask(__name__)

def tsp(graph):
    n = len(graph)
    # memoization table, where dp[mask][i] stores the minimum cost to visit cities in mask, ending in city i
    dp = [[float('inf')] * n for _ in range(1 << n)]
    dp[1][0] = 0

    # For path reconstruction
    parent = [[-1] * n for _ in range(1 << n)]

    # Iterate through all subsets of vertices
    for mask in range(1, 1 << n):
        for u in range(n):
            # Continue if u is not in the subset represented by mask
            if not (mask & (1 << u)):
                continue

            # Try to find the shortest path to u from any vertex v
            for v in range(n):
                if mask & (1 << v) and u != v and dp[mask][u] > dp[mask ^ (1 << u)][v] + graph[v][u]:
                    dp[mask][u] = dp[mask ^ (1 << u)][v] + graph[v][u]
                    parent[mask][u] = v

    # Reconstruct the shortest path and compute the minimum cost
    mask = (1 << n) - 1  # All cities have been visited
    u = 0
    min_cost = min(dp[mask][v] + graph[v][u] for v in range(1, n))

    # Reconstructing the path
    v = min(range(1, n), key=lambda v: dp[mask][v] + graph[v][u])
    path = [0, v]
    costs = [graph[0][v]]
    while mask > 1:
        u, v = v, parent[mask][v]
        mask ^= (1 << u)
        if v != -1:  # To handle the case when we reach the starting point
            path.append(v)
            costs.append(graph[u][v])

    path.reverse()
    costs.reverse()
    return min_cost, path, costs

@app.route('/solve', methods=['POST'])
def solve():
    # Extract graph data from the POST request
    data = request.json
    if "graph" not in data:
        return jsonify({"error": "graph data not provided"}), 400

    flat_graph = data["graph"]
    
    # Convert flat list to matrix
    n = int(len(flat_graph) ** 0.5)
    graph = [flat_graph[i:i+n] for i in range(0, len(flat_graph), n)]

    # Validate input
    if len(graph) != n:
        return jsonify({"error": "Please provide a valid square matrix."}), 400

    min_cost, optimal_path, segment_costs = tsp(graph)
    return jsonify({
        "Minimum Cost": min_cost,
        "Optimal Path": "->".join(map(str, optimal_path)),
        "Segment Costs": segment_costs
    })

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)