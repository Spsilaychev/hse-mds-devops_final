# Running Travelling Salesman problem solver

Note: As discussed with @Verschworer , I would be happy to get +1 for effort :D


## Prerequisites:

Docker: Ensure you have Docker installed.

Minikube & kubectl: Install Minikube and kubectl for local Kubernetes deployment.

Git: Ensure you have Git installed to clone the repository.

## Setup:
### 1. Clone the repository

git clone git clone https://gitlab.com/Spsilaychev/hse-mds-devops_final.git

cd hse-mds-devops_final/

### 2. Start Minikube

minikube start

Optional: Set Docker daemon context to Minikube:

eval $(minikube docker-env)

### 3. Build Docker Images:
3.1. Proper solution:

cd app

cd proper_solution

docker build -t proper_solution -f Dockerfile .


3.2. Fallback solution:

cd ..

cd fallback_solution/

docker build -t fallback_solution -f Dockerfile .


### 4. Deploy minikube

cd ..

cd ..

kubectl apply -f k8s/


## Execution:
### 1. Enter the curlpod to run locally 

(Note: I tried to resolve the IP:<port> issue with ingress, nginx adjustments, port rerouting, bridging VMware Fusion, and tried alternative approach with podman-> kind. However, I couldn't resolve it - can't find the issue with it due to the time constraints and other courses we have in parallel):

kubectl run curlpod --image=radial/busyboxplus:curl -i --tty --rm

### 2. Run the code in root@curlpod. 

Example:

curl -X POST -H "Content-Type: application/json" -d '{"graph": [0, 10, 15, 20, 10, 0, 35, 25, 15, 35, 0, 30, 20, 25, 30, 0]}' proper-solution-service:80/solve

